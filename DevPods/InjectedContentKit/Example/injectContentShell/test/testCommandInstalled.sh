#!/bin/sh

# 检测某个命令是否安装
# @return
# 0 未安装
# 1 安装
function test_command_installed {
	echo "test_command_installed=="
	which_command=`which $1`
	echo $which_command
	if [[ -n  $which_command ]]; then
		echo "$1 command installed"
		return 1
	else
		echo "$1 command not installed"
		return 0
	fi
}

# test
test_command_installed $1
result=$?
echo "result = ${result}"
if [[ $result = 0 ]]; then
	echo "not installed"
	brew install enca || exit 1
else
	echo "installed"
fi