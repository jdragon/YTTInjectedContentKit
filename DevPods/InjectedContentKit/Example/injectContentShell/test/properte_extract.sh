#!/bin/sh
prop_line="@property (nonatomic, copy) void(^block)();"
# prop_line="@property (nonatomic, strong) UILabel* ruleCardTextBox;"
asterisk_seperator_pattern="\*"
if [[ ${prop_line} =~ ${asterisk_seperator_pattern} ]]; then
	# 从左向右截取最后一个string后的字符串
	prop_name=${prop_line##*${asterisk_seperator_pattern}}
	# 从左向右截取第一个string后的字符串
	seal_pattern=";*"
	seal_pattern_replacement=""
	prop_name=${prop_name//${seal_pattern}/${seal_pattern_replacement}}
	subsring_pattern="[ |;]"
	replacement=""
	prop_name=${prop_name//${subsring_pattern}/${replacement}}

	echo "prop_name=${prop_name}"

	if [[ ${param_should_use_filter} -gt 0 ]]; then
		grep_result=$(grep ${prop_name} ${blacklist_cfg_file})
		echo "grep_result = >>${grep_result}<<"
		custom_grep_result=""
		if [[ -n ${param_custom_filter_file} ]]; then
			custom_grep_result=$(grep ${prop_name} ${param_custom_filter_file})
		fi
		if [[ -n ${grep_result} ]] || [[ -n ${custom_grep_result} ]]; then
			echo "--${prop_name}--存在配置文件中"
		else
			echo "--${prop_name}--XXX不存在配置文件中"

			tmp_props_array[$props_count]=$prop_name
			props_count=$[ props_count + 1 ]
			echo ">>>>>>>result_prop_name=${prop_name}"
		fi
	else
		tmp_props_array[$props_count]=$prop_name
		props_count=$[ props_count + 1 ]
	fi			
fi